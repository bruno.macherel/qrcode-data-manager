import Vue from 'vue'
import Vuex from 'vuex'
import VueAxios from 'vue-axios'
import axios from 'axios'
import config from '@/config.js'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default new Vuex.Store({
  state: {
    authenticated: false,
    token: null,
    qrcodes: []
  },
  mutations: {
    updateToken(state, {token, authenticated}) {
      state.token = token
      state.authenticated = authenticated
    },
    updateQRCodes(state, qrcodes) {
      state.qrcodes = qrcodes
    },
    addQRCode(state, qrcode) {
      state.qrcodes.push(qrcode);
    },
    deleteQRCode(state, index) {
      state.qrcodes.splice(index, 1);
    }
  },
  actions: {
    async updateToken({commit}, evt) {
      if(!evt) return
      const token = evt.target
        ? evt.target.value
        : evt
      let authenticated = false
      try {
        const res = await axios.get(`${config.apiUrl}/users/${token}`)
        const user = res.data
        commit('updateQRCodes', user.qrcodes)
        authenticated = true
      } catch (e) {
        // Nothing to do
      }
      commit('updateToken', {token, authenticated})
    },
    async addQRCode({commit, dispatch}) {
      const qrcode = { name: null, value: null }
      commit('addQRCode', qrcode)
      await dispatch('saveQRCodes')
    },
    async deleteQRCode({commit, dispatch}, index) {
      commit('deleteQRCode', index)
      await dispatch('saveQRCodes')
    },
    async saveQRCodes({state}) {
      if (state.authenticated) {
        await axios.put(
          `${config.apiUrl}/users/${state.token}/qrcodes`,
          state.qrcodes.map(qrcode => ({...qrcode, type: qrcode.type.value || qrcode.type}))
        )
      }
    },
    async updateQRCodes({commit, dispatch}, qrcodes) {
      commit('updateQRCodes', qrcodes)
      await dispatch('saveQRCodes')
    }

  }
})
