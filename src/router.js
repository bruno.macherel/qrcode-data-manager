import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import QRCodes from './views/QRCode-list.vue'
import User from './views/User.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: QRCodes
    },
    {
      path: '/qrcodes',
      name: 'qrcodes',
      component: QRCodes
    },
    {
      path: '/users',
      name: 'user',
      component: User
    },
    {
      path: '/account',
      name: 'account',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
