import Vue from 'vue'
import {
  Vuetify,
  VApp,
  VBtn,
  VCard,
  VCombobox,
  VDialog,
  VFooter,
  VGrid,
  VIcon,
  VInput,
  VList,
  VNavigationDrawer,
  VSelect,
  VTextarea,
  VToolbar,
  transitions
} from 'vuetify'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  components: {
    VApp,
    VBtn,
    VCard,
    VCombobox,
    VDialog,
    VFooter,
    VGrid,
    VIcon,
    VInput,
    VList,
    VNavigationDrawer,
    VSelect,
    VTextarea,
    VToolbar,
    transitions
  },
})
